﻿using Desafio_Meta3.WebUI.Models;
using Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Desafio_Meta3.WebUI.Controllers
{
    public class UsuarioController : Controller
    {
        // GET: Usuario
        public ActionResult Index(string sortOrder, string searchString)
        {
            UsuarioViewModel usuarioViewModel = new UsuarioViewModel
            {
                UsuarioLista = Negocio.ControleUsuario.ObterListaUsuarios()
            };


            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (!String.IsNullOrEmpty(searchString))
            {
                usuarioViewModel.UsuarioLista = usuarioViewModel.UsuarioLista.Where(s => s.Login.Contains(searchString)).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    usuarioViewModel.UsuarioLista = usuarioViewModel.UsuarioLista.OrderByDescending(s => s.Login).ToList();
                    break;
            }

            return View(usuarioViewModel.UsuarioLista);
        }

        // GET: Usuario/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Usuario/Create
        public ActionResult Create()
        {
           
            PessoaFisicaViewModel pessoaFisicaViewModel = new PessoaFisicaViewModel
            {
                PessoaFisicaLista = Negocio.ControlePessoaFisica.ObterListaPessoaFisica()
            };
            
            ViewBag.Pessoas = pessoaFisicaViewModel.PessoaFisicaLista;

            return View();
        }

        // POST: Usuario/Create
        [HttpPost]
        public ActionResult Create(Usuario usuario)
        {
            try
            {
                Negocio.ControleUsuario.InserirUsuario(usuario);

                PessoaFisicaViewModel pessoaFisicaViewModel = new PessoaFisicaViewModel
                {
                    PessoaFisicaLista = Negocio.ControlePessoaFisica.ObterListaPessoaFisica()
                };

                ViewBag.Pessoas = pessoaFisicaViewModel.PessoaFisicaLista;
                ViewBag.sucesso = "Registro salvo com sucesso";
                return View();
            }
            catch (Exception e)
            {
                ViewBag.error = e.Message;
                return View();
            }
        }

        // GET: Usuario/Edit/5
        public ActionResult Edit(int id)
        {
            Usuario usuario = Negocio.ControleUsuario.ObterUsuarioPorId(id);

            PessoaFisicaViewModel pessoaFisicaViewModel = new PessoaFisicaViewModel
            {
                PessoaFisicaLista = Negocio.ControlePessoaFisica.ObterListaPessoaFisica()
            };

            ViewBag.Pessoas = pessoaFisicaViewModel.PessoaFisicaLista;

            return View(usuario);
        }

        // POST: Usuario/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Usuario usuario)
        {
            try
            {
                Negocio.ControleUsuario.AtualizarUsuario(usuario);

                PessoaFisicaViewModel pessoaFisicaViewModel = new PessoaFisicaViewModel
                {
                    PessoaFisicaLista = Negocio.ControlePessoaFisica.ObterListaPessoaFisica()
                };

                ViewBag.Pessoas = pessoaFisicaViewModel.PessoaFisicaLista;
                ViewBag.sucesso = "Registro salvo com sucesso";
                return View();
            }
            catch (Exception e)
            {
                ViewBag.error = e.Message;
                return View();
            }
        }

        // GET: Usuario/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Usuario/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Negocio.ControleUsuario.DeletarUsuario(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
