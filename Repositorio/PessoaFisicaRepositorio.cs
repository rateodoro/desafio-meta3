﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo;

namespace Repositorio
{
    public static class PessoaFisicaRepositorio
    {
        public static List<PessoaFisica> ObterListaPessoaFisica()
        {
            List<PessoaFisica> pessoaFisicaLista = new List<PessoaFisica>();

            using (var conection = Conexao.ConexaoDB.Conectar())
            {
                var sql = Scripts.PessoaFisica.ObterListaPessoaFisica;

                SqlCommand cmd = new SqlCommand(sql, conection);

                using (var rd = cmd.ExecuteReader())
                {
                    while (rd.Read())
                    {
                        pessoaFisicaLista.Add(new PessoaFisica
                        {
                            IdPessoaFisica = Convert.ToInt32(rd["Id"]),
                            Cpf = Convert.ToString(rd["CPF"]),
                            Nome = Convert.ToString(rd["Nome"]),
                            DataNascimento = Convert.ToDateTime(rd["DataNascimento"]),
                            Email = Convert.ToString(rd["Email"]),
                            TelefoneFixo = Convert.ToString(rd["TelefoneFixo"]),
                            TelefoneCelular = Convert.ToString(rd["TelefoneCelular"]),
                            Sexo = Convert.ToString(rd["Sexo"])
                        });
                    }
                }
                return pessoaFisicaLista;

            }
        }

        public static void InserirPessoaFisica(PessoaFisica pessoaFisica)
        {
            try
            {
                using (var conection = Conexao.ConexaoDB.Conectar())
                {
                    var sql = Scripts.PessoaFisica.InserirPessoaFisica;
                    SqlCommand cmd = new SqlCommand(sql, conection);

                    cmd.Parameters.AddWithValue("@Cpf", pessoaFisica.Cpf);
                    cmd.Parameters.AddWithValue("@Nome", pessoaFisica.Nome);
                    cmd.Parameters.AddWithValue("@DataNascimento", pessoaFisica.DataNascimento);
                    cmd.Parameters.AddWithValue("@Email", !string.IsNullOrEmpty(pessoaFisica.Email) ? pessoaFisica.Email: "" );
                    cmd.Parameters.AddWithValue("@TelefoneFixo", !string.IsNullOrEmpty(pessoaFisica.TelefoneFixo) ? pessoaFisica.TelefoneFixo : "");
                    cmd.Parameters.AddWithValue("@TelefoneCelular", !string.IsNullOrEmpty(pessoaFisica.TelefoneCelular) ? pessoaFisica.TelefoneCelular : "");
                    cmd.Parameters.AddWithValue("@Sexo", !string.IsNullOrEmpty(pessoaFisica.Sexo) ? pessoaFisica.Sexo : "");

                    cmd.ExecuteNonQuery();

                }
            }
            catch (Exception e)
            {

            }
        }

        public static void AtualizaPessoaFisica(PessoaFisica pessoaFisica)
        {
            try
            {
                using (var conection = Conexao.ConexaoDB.Conectar())
                {
                    var sql = Scripts.PessoaFisica.AtualizarPessoaFisica;
                    SqlCommand cmd = new SqlCommand(sql, conection);

                    cmd.Parameters.AddWithValue(@"IdPessoaFisica", pessoaFisica.IdPessoaFisica);

                    cmd.Parameters.AddWithValue("@Cpf", pessoaFisica.Cpf);
                    cmd.Parameters.AddWithValue("@Nome", pessoaFisica.Nome);
                    cmd.Parameters.AddWithValue("@DataNascimento", pessoaFisica.DataNascimento);
                    cmd.Parameters.AddWithValue("@Email", !string.IsNullOrEmpty(pessoaFisica.Email) ? pessoaFisica.Email : "");
                    cmd.Parameters.AddWithValue("@TelefoneFixo", !string.IsNullOrEmpty(pessoaFisica.TelefoneFixo) ? pessoaFisica.TelefoneFixo : "");
                    cmd.Parameters.AddWithValue("@TelefoneCelular", !string.IsNullOrEmpty(pessoaFisica.TelefoneCelular) ? pessoaFisica.TelefoneCelular : "");
                    cmd.Parameters.AddWithValue("@Sexo", !string.IsNullOrEmpty(pessoaFisica.Sexo) ? pessoaFisica.Sexo : "");

                    cmd.ExecuteNonQuery();

                }
            }
            catch (Exception e)
            {

            }
        }

        public static void DeletarPessoaFisica (int id)
        {
            try
            {
                using (var conection = Conexao.ConexaoDB.Conectar())
                {
                    var sql = Repositorio.Scripts.PessoaFisica.DeletarPessoaFisica;
                    SqlCommand cmd = new SqlCommand(sql, conection);
                    cmd.Parameters.AddWithValue(@"IdPessoaFisica", id);
                    cmd.ExecuteNonQuery();
                }
            }
            catch
            {

            }
        }

        public static PessoaFisica ObterPessoaFisicaPorId(int id)
        {
            using (var conection = Conexao.ConexaoDB.Conectar())
            {
                var sql = Scripts.PessoaFisica.ObterPessoaFisicaPorId;

                SqlCommand cmd = new SqlCommand(sql, conection);
                cmd.Parameters.Add(new SqlParameter { ParameterName = "@IdPessoaFisica", Value = id });
                PessoaFisica pessoaFisica = null;
                var rd = cmd.ExecuteReader();
                while (rd.Read())
                {
                    pessoaFisica = new PessoaFisica
                    {
                        IdPessoaFisica = Convert.ToInt32(rd["Id"]),
                        Cpf = Convert.ToString(rd["CPF"]),
                        Nome = Convert.ToString(rd["Nome"]),
                        DataNascimento = Convert.ToDateTime(rd["DataNascimento"]),
                        Email = Convert.ToString(rd["Email"] != null ? rd["Email"] : ""),
                        TelefoneFixo = Convert.ToString(rd["TelefoneFixo"] != null ? rd["TelefoneFixo"]: "" ),
                        TelefoneCelular = Convert.ToString(rd["TelefoneCelular"] != null ? rd["TelefoneCelular"] : ""),
                        Sexo = Convert.ToString(rd["Sexo"] != null ? rd["Sexo"] : "")
                    };
                }

                return pessoaFisica;
            }
        }
    }
}
